class ValysException(Exception):
    def __init__(self, title, message):
        super().__init__(message)
        self.title = title
        self.message = message

class CardException(ValysException):
    """Card exception"""
    def __init__(self, title, message):
        super().__init__(title, message)

class BoxException(ValysException):
    """Box exception"""
    def __init__(self, title, message):
        super().__init__(title, message)

class ConfigException(ValysException):
    def __init__(self, title, message):
        super().__init__(title, message)
