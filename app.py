import os, sys

from PySide2 import QtWidgets as qtw
from PySide2.QtCore import Qt, Slot, QAbstractItemModel, QFile, QTextStream, QIODevice, QSize
from PySide2 import QtGui as qtg
import logging, logging.config

import Services.controller as vl
from Interface.MainWindow import MainWinMar

LG_SETTINGS = "lang_fr.json" #Config Language
UI_SETTINGS = "styleValys.qss"


if __name__ == '__main__':
    # Launch Controller
    controller = vl.Controller()

    # Launch Logger
    logging.basicConfig(level=logging.INFO,
        format = '%(asctime)s - %(name)s - %(levelname)s - %(module)s : %(lineno)d - %(message)s',
        handlers = [logging.handlers.TimedRotatingFileHandler(os.environ["LOGFILE"], when="h", interval = 10, backupCount = 5)])

    logging.getLogger('geventwebsocket.handler').setLevel(logging.ERROR)

    # Create the Qt Application
    app = qtw.QApplication(sys.argv)

    # Create and show the form
    form = MainWinMar(controller)
    form.show()
    stream = QFile(UI_SETTINGS)
    stream.open(QIODevice.ReadOnly)
    app.setStyleSheet(QTextStream(stream).readAll())

    # Run the main Qt loop
    app.exec_()
