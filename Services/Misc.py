import sqlalchemy as db
import os

from Database.essential import Base

class DatabaseLauncher:
    def __init__(self):
        self.dbEngine = db.create_engine(os.environ["DATABASE"])
        session_factory = db.orm.sessionmaker(bind = self.dbEngine, autocommit = False, autoflush = False)
        self.session = session_factory()
        self._setTable()

    def _setTable(self):
        # We create a new table of users
        Base.metadata.create_all(self.dbEngine)
        
class DateParser:
    def __init__(self, date):
        date = date.split("/")
        self.day = date[0]
        self.month = date[1]
        self.year = date[2]

    def toString(self):
        return "{}/{}/{}".format(self.day, self.month, self.year)
