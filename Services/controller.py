from PySide2 import QtWidgets as qtw
import os

import Services.Config as cfg
import Services.Misc as msc
import exceptions as exp

#Logging utility
import logging
logger = logging.getLogger(__name__)

class Controller:
    def __init__(self, debug = False):
        if not debug: self.config = cfg.Config()
        self.database = msc.DatabaseLauncher()
        self.importModule()

    def dateParser(self, date):
        return msc.DateParser(date)

    def importModule(self):
        if os.environ["MODULE"] == "fantasy":
            import Fantasy.StateManagement as bm
            import Fantasy.HeroManagement as cm
            self.boxes = bm.StateManagement(self.database.session)
            self.cards = cm.HeroManagement(self.database.session)

        if os.environ["MODULE"] == "default":
            import Model.BoxManagement as bm
            import Model.CardManagement as cm
            self.boxes = bm.BoxManagement(self.database.session)
            self.cards = cm.CardManagement(self.database.session)

    def open(self, interface, cardToOpen):
        """The open function is a method to open a card with the given name in the output Widget.
            - cardToOpen [str]: The name of the card to be opened.
        Exceptions
            - ValysException: If there is an error while trying to open the card, a ValysException is raised with an error message.
        Side Effects
            The contents of the card are displayed in the output widget.
            The currentCard attribute of the parent object is set to the name of the opened card.
            The timestamp of the card is displayed in the main status bar.
        Notes
            If a card is already open and the user tries to open a new card, a warning message is displayed asking if
            they want to discard any unsaved changes and open the new card. If the user chooses not to open the new card, the function returns without doing anything.
            If the card does not exist, a ValysException is raised with an error message."""

        try:
            ficheTmp = self.cards.open(cardToOpen)

        except exp.ValysException as e:
            qm = qtw.QMessageBox
            qm.critical(interface, e.title, e.message, qm.Ok)
        
        return ficheTmp

    def delete(self, interface, isBox, itemToDelete):
        """Deletes a Box or Card.

        Args:
            itemToDelete (str): The name of the item to delete.
            isBox (bool): Whether the item to delete is a Box (True) or a Card (False).

        Raises:
            ValysException: If an error occurs while deleting the item.
        """
        try:
            if isBox: self.boxes.delete(itemToDelete)
            else: self.cards.delete(itemToDelete)

        except exp.ValysException as e:
            qm = qtw.QMessageBox
            qm.critical(interface, e.title, e.message, qm.Ok)
    
    def move(self, interface, itemToMove, newLocation, isBox):
        """Moves a Box or Card to a new location.

        Args:
            itemToMove (str): The name of the item to move.
            newLoc (str): The name of the new location for the item.
            isBox (bool): Whether the item to move is a Box (True) or a Card (False).

        Raises:
            ValysException: If an error occurs while moving the item.
        """
        try:
            if isBox: self.boxes.move(itemToMove, newLocation)
            else: self.cards.move(itemToMove, newLocation)

        except exp.ValysException as e:
            qm = qtw.QMessageBox
            qm.critical(interface, e.title, e.message, qm.Ok)
    
    def create(self, interface, isBox, name, kwargs: dict):
        """Creates a new Box or Card.

        Args:
            isBox (bool): Whether to create a Box (True) or a Card (False).
            name (str): The name of the item to create.
            description (str, optional): The description of the Box to create. Only applies if isBox is True.
            parent (str, optional): The name of the parent Box for the item.

        Raises:
            ValysException: If an error occurs while creating the item.
        """
        try:
            if not isBox:
                if "box" in kwargs.keys() and kwargs["box"] is not None:
                    kwargs["box"] = self.boxes.get(kwargs["box"])
                self.cards.create(name, **kwargs)
            else: self.boxes.create(name, **kwargs)

        except exp.ValysException as e:
            qm = qtw.QMessageBox
            qm.critical(interface, e.title, e.message, qm.Ok)
    
    def get(self, interface, isBox, name = None):
        """Gets a Box or Card by name.

        Args:
            isBox (bool): Whether to get a Box (True) or a Card (False).
            name (str): The name of the item to get.

        Returns:
            dict: A dictionary representation of the Box or Card.

        Raises:
            ValysException: If an error occurs while getting the item.
        """
        try:
            if not isBox:
                card = self.cards.get(name)
                card = card.as_dict() if not isinstance(card, list) else [i.as_dict() for i in card]
                return card
            else:
                box = self.boxes.get(name)
                box = box.as_dict() if not isinstance(box, list) else [i.as_dict() for i in box]
                return box

        except exp.ValysException as e:
            qm = qtw.QMessageBox
            qm.critical(interface, e.title, e.message, qm.Ok)
    
    def modify(self, interface, isBox, name, kwargs):
        """Modifies the name and/or description of a Box or Card.

        Args:
            isBox (bool): Whether the item to modify is a Box (True) or a Card (False).
            name (str): The current name of the item.
            newName (str): The new name for the item. If not provided (aka str of len 0), the name is not modified.
            newDescr (str, optional): The new description for the item. If not provided, the description is not modified.

        Raises:
            ValysException: If an error occurs during the modification.
        """
        try:
            if not isBox: self.cards.modify(name, **kwargs)
            else: self.boxes.modify(name, **kwargs)

        except exp.ValysException as e:
            qm = qtw.QMessageBox
            qm.critical(interface, e.title, e.message, qm.Ok)