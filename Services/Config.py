import os
import configparser
import exceptions as exp

#Logging utility
import logging
logger = logging.getLogger(__name__)

class Config:
    def __init__(self):
        self.configFilename = 'config.ini'
        self.load()

    def get(self):
        """Get the configuration file for the application."""

        if not os.path.isfile(self.configFilename):
            logger.warning("Configuration doesn't exist: Generating new config file")
            config = self.reset()

        try:
            config = configparser.ConfigParser()
            config.read(self.configFilename)
        except Exception as e:
            logger.warning("Configuration Problem: Parsing failed. Recreating Configuration")
            try:
                config = self.reset()

            except Exception as e:
                logger.error("Configuration Error: File corrupted")
                raise exp.ConfigException("Configuration Problem","Parsing failed. Configuration Corrupted")

        return config

    def load(self):
        """Load the configuration file for the application.

        If the configuration file does not exist, a new one will be generated.
        If there is a problem parsing the existing configuration file, a new one will be generated.

        Returns:
            config (ConfigParser): The loaded configuration file as a ConfigParser object.
        """

        config = self.get()

        # Loading Default
        os.environ["DATABASE"] = config["DEFAULT"]["database"]
        os.environ["MODULE"] = config["DEFAULT"]["module"]
        os.environ["ALL_MODULES"] = config["DEFAULT"]["all_modules"]

        # Loading GUI
        os.environ["STYLE"] = config["GUI"]["stylesheet"]
        os.environ["FONTSIZE"] = config["GUI"]["fontsize"]

        # Loading Logger
        os.environ["LOGFILE"] = config["LOGGER"]["logfile"]

    def reset(self):
        """Method initializing the configuration. Called once at the very installation of the program"""

        config = configparser.ConfigParser()
        config['DEFAULT'] = {'database': 'sqlite:///valys.db',
                            'module' : 'default',
                            'all_modules' : '["default", "fantasy"]'}
        config['GUI'] = {'stylesheet': 'styleValys.qss',
                        'fontsize' : [7, 8, 9, 10, 11, 12, 13, 14, 18, 24, 36, 48, 64, 72, 96, 144, 288]}
        config['LOGGER'] = {'logfile': 'logs/valys.log'}
        with open(self.configFilename, 'w') as configfile:
            config.write(configfile)

        logger.info("Created config file ({})...".format(self.configFilename))

        if "logs" not in os.listdir():
            os.makedirs("logs")
            logger.info("Creating log folder ({})...".format("logs"))
        return config

    def modify(self, config: configparser.ConfigParser ):
        """Method modifying the configuration file.
            - config [ConfigParser] : The new configuration to write in the file"""
        logger.info("Modifying config file ({})".format(self.configFilename))

        with open(self.configFilename, 'w') as configfile:
            config.write(configfile)
