from PySide2 import QtWidgets as qtw
from PySide2 import QtGui as qtg
from PySide2.QtCore import Qt, Slot, QSize
import os
import os.path as pth

from Interface.MainWidget import MainWinWidget
from Interface.DialogWidget import HelpWindow, SettingsWindow
from Interface.CardRenderWidget import BoxTreeView

class VirtualInterface(qtw.QMainWindow):
    def __init__(self):
        super().__init__()
        if os.environ["MODULE"] == "fantasy":
            from Fantasy.FantasyWidgets import CreateDg, ModifyDg, OpenDg, OutputWidg

        if os.environ["MODULE"] == "default":
            from Interface.SecondaryWidgets import CreateDg, ModifyDg, OpenDg, OutputWidg

        self.createDg = CreateDg
        self.modifyDg = ModifyDg
        self.openDg = OpenDg
        self.outputWidg = OutputWidg

    def modDialog(self, *args):
        return self.modifyDg(*args)

    def createDialog(self, *args):
        return self.createDg(*args)

    def openDialog(self, *args):
        return self.openDg(*args)

class MainWinMar(VirtualInterface):
    def __init__(self, controller):
        super().__init__()

            #Paramétrage général
        self.cont = controller

            #Création des widgets
        self.widget = MainWinWidget(self, self.outputWidg)
        self.boxTree = None
        self.currentCard = None

            #Création de la fenêtre et de son pourtour
        self.window().setWindowTitle("Valys") #Gestionnaire de Notes Atlantica
        self.setMinimumSize(500, 500)
        self.resize(1300, 900)
        self.mainMenu = self.menuBar()
        self.mainStatBar = self.statusBar()

            #Création de la barre du menu
        self.menu = [None for i in range(5)]
        self.menu[0] = self.mainMenu.addMenu("&Edition")
        self.menu[1] = self.mainMenu.addMenu("&Fiches")
        self.menu[2] = self.mainMenu.addMenu("&Boites")
        self.menu[3] = self.mainMenu.addMenu("&Format")
        self.menu[4] = self.mainMenu.addMenu("&Autres")

            #Création des sous-menus pour l'édition
        connect_action = [None for i in range(8)]
        connect_action[0] = qtw.QAction("Créer Fiche", self)
        connect_action[0].triggered.connect(lambda : self.add(False))
        connect_action[1] = qtw.QAction("Créer Boite", self)
        connect_action[1].triggered.connect(lambda : self.add(True))
        connect_action[2] = qtw.QAction("Déplacer Fiche", self)
        connect_action[2].triggered.connect(lambda : self.move(False))
        connect_action[3] = qtw.QAction("Déplacer Boite", self)
        connect_action[3].triggered.connect(lambda : self.move(True))

        connect_action[4] = qtw.QAction("Sauvegarder", self)
        connect_action[4].triggered.connect(lambda : self.save(False))
        connect_action[4].setShortcut("Ctrl+S")
        connect_action[5] = qtw.QAction("Sauver et Quitter", self)
        connect_action[5].triggered.connect(lambda : self.save(True))
        connect_action[6] = qtw.QAction("Supprimer Fiche", self)
        connect_action[6].triggered.connect(lambda : self.delete(False))
        connect_action[7] = qtw.QAction("Supprimer Boite", self)
        connect_action[7].triggered.connect(lambda : self.delete(True))

        for i in range(len(connect_action)):
            self.menu[0].addAction(connect_action[i])
            if i == 1 or i == 3 or i == 5: self.menu[0].addSeparator()

        fiche_action = [None, None, None]
        fiche_action[0] = qtw.QAction("Modifier", self)
        fiche_action[0].triggered.connect(lambda: self.modify(False))
        fiche_action[1] = qtw.QAction("Ouvrir", self)
        fiche_action[1].triggered.connect(self.open)
        fiche_action[1].setShortcut("Ctrl+O")
        fiche_action[2] = qtw.QAction("Archiver", self)
        fiche_action[2].triggered.connect(self.gui_archive)

        for i in fiche_action:
            self.menu[1].addAction(i)

        boite_action = [None]
        boite_action[0] = qtw.QAction("Modifier", self)
        boite_action[0].triggered.connect(lambda : self.modify(True))

        for i in boite_action:
            self.menu[2].addAction(i)

        autre_action = [None, None]
        autre_action[0] = qtw.QAction("Aide", self)
        autre_action[0].triggered.connect(self.help)
        autre_action[1] = qtw.QAction("Settings", self)
        autre_action[1].triggered.connect(self.settings)

        for i in autre_action:
            self.menu[4].addAction(i)

            #Création de la toolbar de la gestion des fiches
        sheet_toolbar = qtw.QToolBar("Fiches")
        sheet_toolbar.setIconSize(QSize(14, 14))
        self.addToolBar(sheet_toolbar)

        self.boxView = qtw.QAction(qtg.QIcon(pth.join('Graphics', 'tree.png')), "Ouvrir l'arborescence", self)
        self.boxView.setStatusTip("Ouvrir l'arborescence")
        self.boxView.setCheckable(True)
        self.boxView.setChecked(True)
        self.boxView.triggered.connect(self.docker)
        self.docker()
        self.menu[1].addAction(self.boxView)
        sheet_toolbar.addAction(self.boxView)

            #Création de la toolbar d'édition
        format_toolbar = qtw.QToolBar("Format")
        format_toolbar.setIconSize(QSize(16, 16))
        self.addToolBar(format_toolbar)

        self.fontsize = qtw.QComboBox()
        self.fontsize.addItems([str(s) for s in eval(os.environ["FONTSIZE"])])
        self.fontsize.currentIndexChanged[str].connect(lambda s: self.widget.output.output.setFontPointSize(float(s)))
        format_toolbar.addWidget(self.fontsize)

        self.fonts = qtw.QFontComboBox()
        self.fonts.currentFontChanged.connect(self.widget.output.output.setCurrentFont)
        format_toolbar.addWidget(self.fonts)

        self.bold_action = qtw.QAction(qtg.QIcon(pth.join('images', 'edit-bold.png')), "Bold", self)
        self.bold_action.setStatusTip("Bold")
        self.bold_action.setShortcut(qtg.QKeySequence.Bold)
        self.bold_action.setCheckable(True)
        self.bold_action.toggled.connect(lambda x: self.widget.output.output.setFontWeight(qtg.QFont.Bold if x else qtg.QFont.Normal))
        format_toolbar.addAction(self.bold_action)
        self.menu[3].addAction(self.bold_action)


        self.italic_action = qtw.QAction(qtg.QIcon(pth.join('images', 'edit-italic.png')), "Italic", self)
        self.italic_action.setStatusTip("Italic")
        self.italic_action.setShortcut(qtg.QKeySequence.Italic)
        self.italic_action.setCheckable(True)
        self.italic_action.toggled.connect(self.widget.output.output.setFontItalic)
        format_toolbar.addAction(self.italic_action)
        self.menu[3].addAction(self.italic_action)

        self.underline_action = qtw.QAction(qtg.QIcon(pth.join('images', 'edit-underline.png')), "Underline", self)
        self.underline_action.setStatusTip("Underline")
        self.underline_action.setShortcut(qtg.QKeySequence.Underline)
        self.underline_action.setCheckable(True)
        self.underline_action.toggled.connect(self.widget.output.output.setFontUnderline)
        format_toolbar.addAction(self.underline_action)
        self.menu[3].addAction(self.underline_action)

            #Création d'un layout
        self.setCentralWidget(self.widget)

    @Slot()
    def docker(self):
        if self.boxView.isChecked():
            self.boxTree = BoxTreeView(self)
            self.addDockWidget(Qt.LeftDockWidgetArea, self.boxTree)
        else:
            self.boxTree.close()

    @Slot()
    def add(self, isBox):
        wid = self.createDialog(self, isBox)
        wid.exec()

    @Slot()
    def modify(self, isBox):
        wid = self.modDialog(self, isBox)
        wid.exec()

    @Slot()
    def save(self, quit):
        self.widget.modify(False, self.currentCard, {"content" : self.widget.output.output.toHtml()})
        if quit:
            self.currentCard = None
            self.widget.output.reset()

    @Slot()
    def delete(self, isBox):
        delItem = self.openDialog(self.widget, self.cont, True, isBox)
        delItem.exec()

    @Slot()
    def open(self):
        ficheRch = self.openDialog(self.widget, self.cont)
        ficheRch.exec()

    @Slot()
    def move(self, isBox):
        ficheRch = self.openDialog(self.widget, self.cont, False, isBox, True)
        ficheRch.exec()

    @Slot()
    def help(self):
        help = HelpWindow(self)
        help.exec()

    @Slot()
    def settings(self):
        sets = SettingsWindow(self)
        sets.exec()

    @Slot()
    def gui_archive(self):
        qm = qtw.QMessageBox
        warningOpen = qm.question(self,'Archivage', "Voulez-vous créer une archive de toutes les fiches? \n Ceci ecrasera la précédente sauvegarde sous le nom de \"Valys.zip\"", qm.Yes | qm.No)
        if warningOpen != qm.Yes: return
        print("Not created yet!")
        #self.cont.archive()
