from PySide2 import QtWidgets as qtw
from PySide2 import QtGui as qtg
from PySide2.QtCore import Qt, Slot

from Interface.SecondaryWidgets import OutputWidg, LinksWidg
import exceptions as exp

class MainWinWidget(qtw.QWidget):
    """
    Widget central: The MainWinWidget class is responsible for displaying cards in the main window of the application.
    It contains an output widget, which is used to display the contents of a card, and a links widget,
    which displays a list of links to other cards.
    It essentially is the controller of views, managing nearly all interaction with the controller of services

    Attributes:
        parent (QWidget): The parent widget of the MainWinWidget.
        output (OutputWidg): An instance of the OutputWidg class that is used to display the contents of a card.
        links (LinksWidg): An instance of the LinksWidg class that displays a list of links to other cards.

    Methods:
        __init__: The constructor of the MainWinWidget class. It initializes the widget and sets up its layout.
        open : The open function is a method to open a card with the given name in the output Widget.
    """
    def __init__(self, parent, widgetOutput):
        super(MainWinWidget, self).__init__()
        self.parent = parent
        self.cont = parent.cont
        layout = qtw.QVBoxLayout()

        layoutO = qtw.QHBoxLayout()

        self.links = LinksWidg(self)
        self.output = widgetOutput(self)
        layoutO.addWidget(self.output)
        layoutO.addWidget(self.links)
        layout.addLayout(layoutO)
        self.setLayout(layout)

    def _updateDocker(self):
        """Method that update the docker widget in the Main windows with the modifications added here"""
        if self.parent.boxView.isChecked(): self.parent.boxTree.boxView.initCreateTree()

    def open(self, cardToOpen):
        """The open function is a method to open a card with the given name in the output Widget.
            - cardToOpen [str]: The name of the card to be opened.
        Exceptions
            - ValysException: If there is an error while trying to open the card, a ValysException is raised with an error message.
        Side Effects
            The contents of the card are displayed in the output widget.
            The currentCard attribute of the parent object is set to the name of the opened card.
            The timestamp of the card is displayed in the main status bar.
        Notes
            If a card is already open and the user tries to open a new card, a warning message is displayed asking if
            they want to discard any unsaved changes and open the new card. If the user chooses not to open the new card, the function returns without doing anything.
            If the card does not exist, a ValysException is raised with an error message."""

        if self.parent.currentCard is not None:
            qm = qtw.QMessageBox
            warningOpen = qm.question(self,'Ouverture', "Voulez-vous ouvrir une nouvelle fiche? (Toute donnée non sauvegardée sera perdu!)", qm.Yes | qm.No)
            if warningOpen != qm.Yes: return

        self.parent.currentCard = cardToOpen
        self.output.setEnabled(True)

        fiche = self.cont.open(self, cardToOpen)
        self.output.setText(cardToOpen, fiche)
        self.parent.mainStatBar.showMessage("Creation : {}    ||     Last Modification : {}".format(
                    fiche["dt_creation"].strftime('%d-%m-%Y | %H:%M'),
                    fiche["dt_lastmod"].strftime('%d-%m-%Y | %H:%M:%S')))
        
        self._updateDocker()

    def delete(self, itemToDelete, isBox):
        """Deletes a Box or Card.

        Args:
            itemToDelete (str): The name of the item to delete.
            isBox (bool): Whether the item to delete is a Box (True) or a Card (False).

        Raises:
            ValysException: If an error occurs while deleting the item.
        """
        qm = qtw.QMessageBox
        if not isBox: warningText = "Etes-vous sûr de vouloir supprimer la fiche {}".format(itemToDelete)
        else: warningText = "Etes-vous sûr de vouloir supprimer la boite {} et tout ce qu'elle contient".format(itemToDelete)
        warningOpen = qm.question(self,'Suppression', warningText, qm.Yes | qm.No)
        if warningOpen != qm.Yes: return
        
        self.cont.delete(self, isBox, itemToDelete)
       
        self._updateDocker()

    def move(self, itemToMove, newLoc, isBox):
        """Moves a Box or Card to a new location.

        Args:
            itemToMove (str): The name of the item to move.
            newLoc (str): The name of the new location for the item.
            isBox (bool): Whether the item to move is a Box (True) or a Card (False).

        Raises:
            ValysException: If an error occurs while moving the item.
        """
        self.cont.move(self, itemToMove, newLoc, isBox)

        self._updateDocker()

    def create(self, isBox, name, kwargs: dict):
        """Creates a new Box or Card.

        Args:
            isBox (bool): Whether to create a Box (True) or a Card (False).
            name (str): The name of the item to create.
            description (str, optional): The description of the Box to create. Only applies if isBox is True.
            parent (str, optional): The name of the parent Box for the item.

        Raises:
            ValysException: If an error occurs while creating the item.
        """
        self.cont.create(self, isBox, name, kwargs)
        self._updateDocker()

    def get(self, isBox, name = ""):
        """Gets a Box or Card by name.

        Args:
            isBox (bool): Whether to get a Box (True) or a Card (False).
            name (str): The name of the item to get.

        Returns:
            dict: A dictionary representation of the Box or Card.

        Raises:
            ValysException: If an error occurs while getting the item.
        """
        if name == "": name = None
        return self.cont.get(self, isBox, name)

    def modify(self, isBox, name, kwargs: dict):
        """Modifies the name and/or description of a Box or Card.

        Args:
            isBox (bool): Whether the item to modify is a Box (True) or a Card (False).
            name (str): The current name of the item.
            newName (str): The new name for the item. If not provided (aka str of len 0), the name is not modified.
            newDescr (str, optional): The new description for the item. If not provided, the description is not modified.

        Raises:
            ValysException: If an error occurs during the modification.
        """
        if len(name) == 0: name = None
        for i in kwargs.keys():
            if isinstance(kwargs[i], str) and len(kwargs[i]) == 0: kwargs[i] = None

        self.cont.modify(self, isBox, name, kwargs)
        self._updateDocker()
