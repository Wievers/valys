from PySide2 import QtWidgets as qtw
from PySide2.QtCore import Qt, Slot
from PySide2 import QtGui as qtg

class BoxTree(qtw.QTreeWidget):
    def __init__(self, parent):
        super(BoxTree, self).__init__(parent)
        self.parent = parent
        self.cont = self.parent.parent.cont
        self.setColumnCount(1)
        self.setHeaderLabels(["Cards"])
        self.initCreateTree()
        self.itemDoubleClicked.connect(self.openItem)

    def _createTree(self, root, items):
        for item in items:
            # Create a QTreeWidgetItem for the item
            tree_item = qtw.QTreeWidgetItem(root)
            tree_item.setText(0, item.name)
            self.expandItem(tree_item)

            # Add the item's children to the tree
            if item in self.cont.boxes:
                children = self.cont.boxes.get_children(item.name)
            else:
                children = []

            if children:
                self._createTree(tree_item, children)

    def initCreateTree(self):
        items = self.cont.boxes.get_children()
        self.clear()
        self._createTree(self.invisibleRootItem(), items)

    @Slot()
    def openItem(self, name):
        nameCard = name.text(0)
        self.parent.parent.widget.open(nameCard)

    def addBranch(self, name, parent):
        pass
        #items[name] = qtw.QTreeWidgetItem(item[parent])

    def delBranch(self, name):
        pass
        #self.takeTopLevelItem(tree.indexOfTopLevelItem(i))

class BoxTreeView(qtw.QDockWidget):
    def __init__(self, parent):
        super(BoxTreeView, self).__init__(parent)
        self.parent = parent
        self.setMinimumSize(130, 50)
        self.setFloating(False)
        self.setFeatures(qtw.QDockWidget.NoDockWidgetFeatures)

        self.boxView = BoxTree(self)

        self.setWidget(self.boxView)
