from PySide2 import QtWidgets as qtw
from PySide2 import QtGui as qtg
from PySide2.QtCore import Qt, Slot

class OpenDg(qtw.QDialog):
    """Class asking the input to find the sheet to open"""
    def __init__(self, parent, controller, delete = False, box = False, move = False):
        super().__init__(parent, modal = True)
        self.setMinimumSize(300, 30)
        layout = qtw.QVBoxLayout()
            # Attributes
        self.parent = parent
        self.cont = controller
        self.box = box
        self.researchBar = qtw.QComboBox()

            #Formatting names for the GUI appearance
        cardsTmp = self.parent.get(False)
        boxesTmp = self.parent.get(True)
        namesFormatted = [i["name"] for i in cardsTmp] if not box else [i["name"] for i in boxesTmp]

        self.researchBar.addItems(namesFormatted)
        self.researchBar.setEditable(True)
        self.researchBar.completer().setCompletionMode(qtw.QCompleter.PopupCompletion)
        self.researchBar.setCurrentText("")

        if delete:
            group = qtw.QGroupBox("Suppression")
            self.research = qtw.QPushButton("Supprimer")
            self.research.clicked.connect(self.OS_delete)

        elif move:
            boxesDestination = [i["name"] for i in boxesTmp]
            self.res2Label = qtw.QLabel("Dossier de destination")
            self.resLabel = qtw.QLabel("Fiche à déplacer" if not box else "Boite à déplacer")
            self.researchBar2 = qtw.QComboBox()
            self.researchBar2.addItems(boxesDestination)
            self.researchBar2.setEditable(True)
            self.researchBar2.completer().setCompletionMode(qtw.QCompleter.PopupCompletion)
            self.researchBar2.setCurrentText("")
            group = qtw.QGroupBox("Déplacer")
            self.research = qtw.QPushButton("Déplacer")
            self.research.clicked.connect(self.OS_move)

        else:
            group = qtw.QGroupBox("Ouvrir")
            self.research = qtw.QPushButton("Ouvrir")
            self.research.clicked.connect(self.openSheet)

        if move: layout.addWidget(self.resLabel)
        layout.addWidget(self.researchBar)
        if move:
            layout.addWidget(self.res2Label)
            layout.addWidget(self.researchBar2)
        layout.addWidget(self.research)

        group.setLayout(layout)
        layFin = qtw.QVBoxLayout()
        layFin.addWidget(group)
        self.setLayout(layFin)

    def openSheet(self):
        self.parent.open(self.researchBar.currentText())
        self.close()

    def OS_delete(self):
        self.parent.delete(self.researchBar.currentText(), self.box)
        self.close()

    def OS_move(self):
        self.parent.move(self.researchBar.currentText(), self.researchBar2.currentText(), self.box)
        self.close()

class ModifyDg(qtw.QDialog):
    """Class asking the input to modify the sheet"""
    def __init__(self, parent, box = False):
        super(ModifyDg, self).__init__(parent)
        self.parent = parent
        self.isBox = box

        layout = qtw.QVBoxLayout()

        self.items = qtw.QComboBox()
        self.items.setEditable(True)
        self.items.completer().setCompletionMode(qtw.QCompleter.PopupCompletion)
        self.items.setCurrentText("")
        self.val = qtw.QPushButton("Valider", self)
        self.val.clicked.connect(self.modify)

        self.name = qtw.QLineEdit()
        self.name.setReadOnly(True)
        self.name.setPlaceholderText("Name")

        self.saveBut = qtw.QPushButton("Sauvegarder", self)
        self.saveBut.clicked.connect(self.save)

        layTmp = qtw.QHBoxLayout()
        layTmp.addWidget(self.items)
        layTmp.addWidget(self.val)
        layout.addLayout(layTmp)
        layout.addWidget(self.name)

        if self.isBox:
            boxTmp = self.parent.widget.get(True, "")
            self.items.addItems([i["name"] for i in boxTmp])

            self.descr = qtw.QTextEdit()
            self.descr.setReadOnly(True)
            self.descr.setPlaceholderText("Description")

            layout.addWidget(self.descr)

        else:
            cardTmp = self.parent.widget.get(False, "")
            self.items.addItems([i["name"] for i in cardTmp])


        layout.addWidget(self.saveBut)
        self.setLayout(layout)

    @Slot()
    def modify(self):
        item = self.parent.widget.get(self.isBox, self.items.currentText())
        if item is None: return
        self.name.setReadOnly(False)
        self.name.setText(item["name"])
        if self.isBox:
            self.descr.setReadOnly(False)
            self.descr.setText(item["description"])

        return item

    @Slot()
    def save(self):
        descr = None if not self.isBox else self.descr.toPlainText()
        if self.isBox:
            self.parent.widget.modify(self.isBox, self.items.currentText(), {"newName" : self.name.text(), "newDescr" : descr})
        else:
            self.parent.widget.modify(self.isBox, self.items.currentText(), {"newName" : self.name.text()})

        self.close()

class CreateDg(qtw.QDialog):
    def __init__(self, parent, box = False):
        super(CreateDg, self).__init__(parent)
        self.parent = parent
        self.isBox = box
        self.descr = None

        layout = qtw.QVBoxLayout()
        self.title = qtw.QLineEdit()
        self.title.setPlaceholderText("Titre")
        layout.addWidget(self.title)

        self.boxPater = qtw.QComboBox()
        boxTmp = self.parent.cont.get(self, True)
        self.boxPater.addItems([i["name"] for i in boxTmp])
        self.boxPater.setEditable(True)
        self.boxPater.lineEdit().setPlaceholderText("Parent")
        self.boxPater.completer().setCompletionMode(qtw.QCompleter.PopupCompletion)
        self.boxPater.setCurrentText("")
        layout.addWidget(self.boxPater)

        if self.isBox:
            self.descr = qtw.QTextEdit()
            self.descr.setPlaceholderText("Description")
            layout.addWidget(self.descr)

        self.saveBut = qtw.QPushButton("Sauvegarder", self)
        self.saveBut.clicked.connect(self.save)
        layout.addWidget(self.saveBut)
        self.setLayout(layout)

    @Slot()
    def save(self):
        descr = None if self.descr is None else self.descr.toPlainText()
        boxParent = None if len(self.boxPater.currentText()) == 0 else self.boxPater.currentText()
        title = None if len(self.title.text()) == 0 else self.title.text()
        
        if self.isBox:
            self.parent.widget.create(self.isBox, title, {"descr" : descr, "parent" : boxParent})
        else:
            self.parent.widget.create(self.isBox, title, {"box" : boxParent})
        self.close()

class OutputWidg(qtw.QGroupBox):
    def __init__(self, parent, font = None):
        super(OutputWidg, self).__init__()
        if font is None: font = qtg.QFont('Times', 12)
        self.parent = parent
        layout = qtw.QVBoxLayout()
        self.output = qtw.QTextEdit()
        self.output.setEnabled(False)
        self.output.setAutoFormatting(qtw.QTextEdit.AutoAll)
        #self.output.selectionChanged.connect(self.update_format)
        self.output.setFont(font)
        self.output.setFontPointSize(12)

        layout.addWidget(self.output)
        self.setLayout(layout)

    def reset(self):
        """Method resetting text fields of the group box"""
        self.linkcaster = None
        self.setTitle("")
        self.output.clear()
        self.setEnabled(False)

    def setEnabled(self, enab):
        self.output.setEnabled(enab)

    def setText(self, title, card):
        """Method setting the text of the outputwidget.
            - card [dict] : Doct object of the card"""
        self.setTitle(title)
        self.output.setText(card["content"])

    def toPlainText(self):
        return self.output.toPlainText()

class LinksWidg(qtw.QGroupBox):
    """Class generating the Links widget group box
        parent : The main class creating this instance"""
    def __init__(self, parent):
        super(LinksWidg, self).__init__()
        #Class-Wide parameters
        self.parent = parent
        self.linkcaster = None

        self.setMaximumWidth(150)
        self.setTitle("Liens")
        self.linkTree = qtw.QListWidget()
        #self.linkTree.addItems([qtw.QListWidgetItem(i) for i in items])

        self.addLink = qtw.QPushButton("Ajout Lien", self)
        self.addLink.setEnabled(False)
        self.addLink.clicked.connect(self.createLink)

        layoutS = qtw.QHBoxLayout()
        layoutS.addWidget(self.addLink)
        layout = qtw.QVBoxLayout()
        layout.addWidget(self.linkTree)
        layout.addLayout(layoutS)
        self.setLayout(layout)

    def reset(self):
        """Method resetting text fields of the group box"""
        self.linkcaster = None
        self.linkTree.clear()
        self.addLink.setEnabled(False)

    def addLinks(self, items, linkcaster):
        """Method to create new links for the sheet loaded
            - items : Possible link-targets (Other sheets)
            - linkcaster : The loaded sheet"""
        self.linkcaster = linkcaster
        for i in items:
            self.newItemTree(i)
        self.addLink.setEnabled(True)

    def createLink(self):
        newLink = AddLinkBox(self, list(self.parent.parent.cont.cardDict.keys()), self.linkcaster)
        newLink.exec()
        outputTmp = newLink.result()
        outputDictTmp = {"type" : outputTmp[1], "target" : outputTmp[0]}
        self.newItemTree(outputDictTmp)
        self.parent.parent.cont.modify(self.linkcaster, links = [outputDictTmp])

    def newItemTree(self, item):
        """Method to put new items in the list widget
            - items : dict containing the type and the target of the item"""
        newItem = qtw.QListWidgetItem(item["target"], self.linkTree)
        newItem.setToolTip(item["type"])
