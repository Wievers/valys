from PySide2 import QtWidgets as qtw
from PySide2.QtCore import Qt, Slot
from PySide2 import QtGui as qtg

import os

import exceptions as exp

class VirtualDialog(qtw.QDialog):
    def __init__(self, controller, parent = None, *args):
        super().__init__(parent, *args)
        self.setWindowTitle("Valys Dialog")
        self.cont = controller

class HelpWindow(VirtualDialog):
    """Help and Loicence window"""
    def __init__(self, parent = None):
        super().__init__(parent.cont, parent)
        lang = { "close": "Fermer",
          "edit" : "Edition",
          "format" : "Format",
          "sheet" : "Fiches",
          "save": "Sauvegarder",
          "creator" : "Paul MONKAM-DAVERAT",
          "licence" : "Licenced under GNU GPL 3 or any later version",
          "version" : "Valys Note Manager v2.0 qt GUI",
          "descript" : "https://gitlab.com/Wievers/valys/-/blob/main/README.md"
        }
        layout = qtw.QVBoxLayout()
        content = qtw.QLabel("<html>Name : {0}<br>Description : <a href=\"{1}\"><font color=green>{1}</font></a><br>Creator : {2}<br>Licence : {3}</html>".format(lang["version"], lang["descript"], lang["creator"], lang["licence"]))
        content.setOpenExternalLinks(True)
        closeBtn = qtw.QPushButton(lang["close"])
        closeBtn.setShortcut("X")
        closeBtn.clicked.connect(self.close)

        layout.addWidget(content)
        layout.addWidget(closeBtn)
        self.setLayout(layout)


class SettingsWindow(VirtualDialog):
    def __init__(self, parent = None):
        super().__init__(parent.cont, parent)
        self.setWindowTitle("Valys Settings")
        self.setFixedSize(200, 100)

        self.modules = qtw.QComboBox()
        modules = eval(os.environ["ALL_MODULES"])
        self.modules.addItems(modules)
        self.modules.setEditable(True)
        self.modules.completer().setCompletionMode(qtw.QCompleter.PopupCompletion)
        self.modules.setCurrentText(os.environ["MODULE"])

        apply = qtw.QPushButton("Apply")
        apply.clicked.connect(self.applySets)

        layout = qtw.QVBoxLayout()
        layout.addWidget(self.modules)
        layout.addWidget(apply)
        self.setLayout(layout)

    def applySets(self):
        config = self.cont.config.get()
        config["DEFAULT"]["module"] = self.modules.currentText()
        self.cont.config.modify(config)
        self.close()

    def browse(self):
        files = qtw.QFileDialog()
        files.setOption(qtw.QFileDialog.ShowDirsOnly, True)
        files.setFileMode(qtw.QFileDialog.DirectoryOnly)
        files.exec_()
        self.listWidgetsConf[1].setText(files.directory().absolutePath())

class AddLinkBox(VirtualDialog):
    """Class generating the Links widget dialog prompt
        parent : The main class creating this instance
        items : Possible targets for the new link
        linkcaster : The sheet id that creates the link (linkcaster --> unknown)
        modal : Whether or not this window blocks access to other windows of the program"""
    def __init__(self, parent, items, linkcaster, modal = True):
        super().__init__(parent.cont, parent)
        self.parent = parent
        self.linkcaster = linkcaster

        self.setModal(modal)
        groupLink = qtw.QGroupBox("Ajout d'un lien")
        self.linkNature = qtw.QLineEdit()
        self.linkNature.setPlaceholderText("Type de lien")
        self.addLink = qtw.QPushButton("Ajout")
        self.addLink.clicked.connect(self.close)
        self.linkTo = qtw.QComboBox()
        self.linkTo.addItems([i for i in items if i.strip() != self.linkcaster.strip()])
        self.linkTo.setEditable(True)
        self.linkTo.completer().setCompletionMode(qtw.QCompleter.PopupCompletion)
        self.linkTo.setCurrentText("")
        self.linkTo.setPlaceholderText("Fiche")

        layoutS = qtw.QVBoxLayout()
        layoutS.addWidget(self.linkTo)
        layoutS.addWidget(self.linkNature)
        layoutS.addWidget(self.addLink)
        groupLink.setLayout(layoutS)
        layoutP = qtw.QVBoxLayout()
        layoutP.addWidget(groupLink)
        self.setLayout(layoutP)

    def result(self):
        return self.linkTo.currentText(), self.linkNature.text()
