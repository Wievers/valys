import os
from datetime import datetime as dt
import Database.essential as mod
import exceptions as exp

#Logging utility
import logging
logger = logging.getLogger(__name__)

class BoxManagement(mod.VirtualManagement):
    def __init__(self, *args):
        super().__init__(*args)
        self.managedDb = mod.Box
    
    def __contains__(self, item):
        """Function that checks if a box exists in the database
            - item : The item to match
            - return : True if the box exists, False otherwise"""
        if not isinstance(item, mod.Box): return False
        rawRes = self.session.query(mod.Box).filter(mod.Box.id == item.id)
        return len(list(rawRes)) != 0

    def _delete_recursive(self, box):
        """Helper function to delete a box and all its children recursively"""
        # Delete all children boxes
        children = self.session.query(mod.Card).filter(mod.Card.group_id == box.id)
        for child in children:
            if self.__contains__(child):
                self._delete_recursive(child)
            else:
                logger.info("Card {} successfully deleted.".format(child.name))
                self.session.delete(child)
                self.session.commit()

        # Delete the box
        logger.info("Deleting box {} in the database".format(box.name))
        self.session.delete(box)
        self.session.commit()

    def get(self, name = None, id=None):
        """Function that retrieves all boxes from the database
            - name : The name of the box to match
            - id : The id of the box to match (optional)
            - return : The box matching the name or a list of boxes in the database"""
        if id:
            try:
                query = mod.Box.get(self.session, id = id)
            except ValueError as e:
                logger.error(str(e))
                raise exp.BoxException("Box problem", str(e))

            if query is None:
                logger.error("Box with id {} doesn't exist".format(id))
                raise exp.BoxException("Unfound Box", "Box with id {} doesn't exist".format(id))
            return query

        if name:
            # In this case, we return the on
            try:
                query = mod.Box.get(self.session, name = name)
            except ValueError as e:
                logger.error(str(e))
                raise exp.BoxException("Box problem", str(e))

            if query is None:
                logger.error("Box {} doesn't exist".format(name))
                raise exp.BoxException("Unfound Box", "Box {} doesn't exist".format(name))
            return query

        logger.debug("Retrieved boxes from the database")
        query = self.session.query(mod.Box)
        return query.all()

    def get_children(self, name = None):
        """Method that gets all the children of a given box
            - name : The name of the box"""
        if name is None:
            queryBoxes = self.session.query(mod.Box).filter(mod.Box.parent == None)
            queryCards = self.session.query(mod.Card).filter(mod.Card.group == None)
            return queryBoxes.all() + queryCards.all()

        box = self.get(name)
            
        queryBoxes = self.session.query(mod.Box).filter(mod.Box.parent == box)
        queryCards = self.session.query(mod.Card).filter(mod.Card.group == box)

        return queryBoxes.all() + queryCards.all()

    def create(self, name, descr = None, parent = None):
        """Method to create a new box
            - name : The name of the new box
            - parent_name [str] : The name of the parent box (None if root)"""
        # Find the parent box id
        parent_id = None
        if parent:
            parent_box = self.get(name=parent)
            if not parent_box:
                logger.error("Error: Parent box does not exist.")
                raise exp.BoxException("Box unfound","Error: Parent box does not exist.")

            parent_id = parent_box.id

        # Check if a box with the same name already exists in the same parent box
        query = self.session.query(mod.Box).filter(mod.Box.name == name)
        if parent_id:
            query = query.filter(mod.Box.parent_id == parent_id)

        if query.count() > 0:
            logger.warning("Error: A box with the same name already exists in the same parent box.")
            raise exp.BoxException("Box Already Exists","Error: A box with the same name already exists in the same parent box.")

        # Create the new box
        new_box = mod.Box(name=name, parent_id=parent_id, description = descr)
        self.session.add(new_box)
        self.session.commit()
        logger.debug("Successfully created new box: {}".format(name))
        return new_box

    def modify(self, name, newName = None, newDescr = None):
        """Function that MODIFY a box
            - name : The name of the box to modify
            - newName : The new name of the box (optional)
            - newDescr : The new description of the box (optional)
            - return : The modified box"""
            
        # Check if the card exists
        self.get(name = name)

        # Update the content of the card if provided
        if newDescr:
            self.session.query(mod.Box).filter(mod.Box.name == name).update({mod.Box.description: newDescr})
            self.session.commit()

        if newName:
            self.session.query(mod.Box).filter(mod.Box.name == name).update({mod.Box.name: newName})
            self.session.commit()

    def delete(self, name):
        """Function that DELETE a box
            - name : The name of the box to delete"""
        # Check if the box exists
        box = self.get(name=name)
        if not box:
            logger.error("Error: Box does not exist.")
            raise exp.BoxException("Box Unfound", "Error: Box does not exist.")

        # Delete the box and all its children (recursive delete)
        self._delete_recursive(box)
        self.session.commit()

    def move(self, name, new_group_name):
        """Function that MOVE a box to another box
            - name : The name of the box to move
            - new_group_name : The name of the group to move the box to"""
        # Check if the box exists
        box = self.get(name=name)
        if not box:
            logger.error("Error: Box does not exist.")
            raise exp.BoxException("Box Unfound","Error: Box does not exist.")

        # Check if the new group exists
        new_group = self.get(name=new_group_name)
        if not new_group:
            logger.error("Error: New box does not exist.")
            raise exp.BoxException("Box Unfound","Error: New box does not exist.")

        # Update the group_id of the box
        box.parent_id = new_group.id
        self.session.commit()
