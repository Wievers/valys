import os
from datetime import datetime as dt
import Database.essential as mod
import exceptions as exp

#Logging utility
import logging
logger = logging.getLogger(__name__)

class CardManagement(mod.VirtualManagement):
    """The main class of the project. It manages the cards and the link with potential APIs"""
    def __init__(self, *args):
        super().__init__(*args)
        self.managedDb = mod.Card
        self.boxDb = mod.Box
    
    def __contains__(self, item):
        """Function that checks if a card exists in the database
            - item : The item to match
            - return : True if the card exists, False otherwise"""
        if not isinstance(item, mod.Card): return False
        rawRes = self.session.query(mod.Card).filter(mod.Card.id == item.id)
        return len(list(rawRes)) != 0

    def _get_box(self, box):
        """Helper function to get a box from the database
            - box [str] : The name of the box to get"""
        try:
            parentBox = self.boxDb.get(self.session, name = box)
        except ValueError as e:
            logger.error(str(e))
            raise exp.BoxException("Box problem", str(e))

        if parentBox is None:
            logger.error("Box : {} doesn't exists".format(box))
            raise exp.BoxException("Unknown Box", "Error: Box '{}' does not exist".format(box))

        return parentBox

    def get(self, name = None, id = None):
        """Function that retrieves all cards from the database
            - return : A list of Card objects representing the cards in the database"""

        if name is not None:
            try:
                query = mod.Card.get(self.session, name = name)
            except ValueError as e:
                logger.error(str(e))
                raise exp.CardException("Card problem", str(e))

            if query is None:
                logger.error("Card with name {} doesn't exist".format(name))
                raise exp.CardException("Unfound Card", "Card with name {} doesn't exist".format(name))
            return query

        elif id is not None:
            try:
                query = mod.Card.get(self.session, id = id)
            except ValueError as e:
                logger.error(str(e))
                raise exp.CardException("Card problem", str(e))

            if query is None:
                logger.error("Card with id {} doesn't exist".format(id))
                raise exp.CardException("Unfound Card", "Card with id {} doesn't exist".format(id))
            return query

        query = self.session.query(mod.Card)
        return query.all()

    def open(self, name):
        """Method opening a card
            - name [str] : The name of the card to open
            - return [dict] : The result of the as_dict method of card"""
        # Search for the card
        card = self.get(name=name)
        if not card:
            logger.error("Error: Card <{}> does not exist.".format(name))
            raise exp.CardException("Unfound Card","Error: Card does not exist.")

        # Return the card's content
        return card.as_dict()

    def create(self, title, content = "", box = None):
        """Function that CREATE a card
            - title : The title of the new card, which is the base of the filename of the card
            - content : The content of the new card. It should NOT be empty
            - box [Box]: The box in which the card is"""
        # We check if the card already exists in the database
        rawRes = self.session.query(mod.Card).filter(mod.Card.name == title)
        if len(list(rawRes)) != 0:
            logger.error("Card : {} already exists".format(title))
            raise exp.CardException("Existing Card", "Error: Card {} already exists".format(title))

        # We create a new card
        newCardId = super()._attrId(mod.Card)

        # We add it in the database
        nCard = mod.Card(id = newCardId, name = title, content = content, dtCreation = dt.now(), dtLastMod = dt.now(), links = None, group = box)
        self.session.add(nCard)
        self.session.commit()
        logger.info("Succesfully created card : {}".format(title))

        return nCard

    def modify(self, nameCard, newName = None, content = None, links = None):
        """Function that MODIFY a card
            - nameCard : The title of the new card, which is the base of the filename of the card
            - content : The content of the new card. It should NOT be empty
            - links : list of dict of the type and the target of the new link"""
        # Check if the card exists
        card = self.get(name = nameCard)

        # Update the content of the card if provided
        if content:
            self.session.query(mod.Card).filter(mod.Card.name == nameCard).update({mod.Card.content: content, mod.Card.dtLastMod : dt.now()})
            self.session.commit()

        if newName: #We modify the name last if needed
            self.session.query(mod.Card).filter(mod.Card.name == nameCard).update({mod.Card.name: newName, mod.Card.dtLastMod : dt.now()})
            self.session.commit()

    def move(self, name, newBox):
        """Method to move a card/box in another box
            - name : The name of the item you want to move
            - newBox : The name of the box you want to put your item in"""
        try:
            newBox = self.boxDb.get(self.session, name = newBox)
        except ValueError as e:
            logger.error(str(e))
            raise exp.BoxException("Box problem", str(e))

        if newBox is None:
            logger.error("Box : {} doesn't exist!".format(newBox))
            raise exp.BoxException("Box Unfound","Error: Box doesn't exist!")

        rawRes = self.session.query(mod.Card).filter(mod.Card.name == name).update({mod.Card.group_id: newBox.id})
        self.session.commit()

    def delete(self, name):
        """Method to delete a card
            - name : The name of the item to delete"""
        # Check if the card exists
        cards = self.get(name=name)
        if not cards:
            logger.error("Card : {} doesn't exist!".format(name))
            raise exp.CardException("Card Unfound", "Error: Card does not exist.")

        # Delete the card
        self.session.delete(cards)
        self.session.commit()
        logger.debug("Card {} successfully deleted.".format(name))
