import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.pool import NullPool
import greenlet
import os

Base = declarative_base()

CONFIG = "sqlite:///valys.db"

class VirtualManagement(object):
    instances = {} #This is a static attribute in python to check the instances created.
    def __new__(cls, *args, **kargs):
        # This magic method is a static method, using cls (which is the class object) and verifying that no instances exists at this time.
        if VirtualManagement.instances.get(cls) is None:
            VirtualManagement.instances[cls] = object.__new__(cls)
        return VirtualManagement.instances[cls]

    def __init__(self, sesDb):
        self.session = sesDb
        
    def _attrId(self, table):
        rawRes = self.session.query(table).filter(table.id)
        if len(list(rawRes)) == 0: return 1
        else:
            return max(rawRes, key = lambda x:x.id).id + 1

class VirtualDatabase:
    @classmethod
    def get(cls, session, id=None, name=None):
        if id:
            # Query the database for a Box with the specified id
            item = session.query(cls).filter_by(id=id).first()
        elif name:
            # Query the database for a Box with the specified name
            item = session.query(cls).filter_by(name=name).first()
        else:
            # Neither an id nor a name was specified
            raise ValueError("Must specify either a name (or an id) to retrieve a Card")

        return item

class Card(VirtualDatabase, Base):
    __tablename__ = "cards"

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(255), nullable = False)
    content = db.Column(db.String(255), nullable = False)
    dtCreation = db.Column(db.DateTime, nullable = False)
    dtLastMod = db.Column(db.DateTime, nullable = False)
    links = db.Column(db.Integer, nullable = True)
    group_id = db.Column(db.Integer, db.ForeignKey("boxes.id"))
    group = db.orm.relationship("Box", backref=db.orm.backref("cards", lazy=True))

    def as_dict(self):
        return {
            'id' : self.id,
            'name': self.name,
            'content': self.content,
            'dt_creation' : self.dtCreation,
            'dt_lastmod' : self.dtLastMod,
            'links': self.links,
            'group' : None if self.group is None else self.group.name
        }

class Box(VirtualDatabase, Base):
    __tablename__ = "boxes"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable = True)
    parent_id = db.Column(db.Integer, db.ForeignKey('boxes.id'))
    parent = db.orm.relationship('Box', remote_side=[id], backref='children')

    def as_dict(self):
        return {
            'id' : self.id,
            'name': self.name,
            'description': self.description,
            'parent' : None if self.parent is None else self.parent.name
        }

class Link(Base):
    __tablename__ = "links"

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(255), nullable = False)
    description = db.Column(db.String(255), nullable = False)

    def as_dict(self):
        return {
            'id' : self.id,
            'name': self.name,
            'description' : self.description
        }
