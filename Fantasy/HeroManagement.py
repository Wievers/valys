import os
from datetime import datetime as dt

import Fantasy.FS_Database as mod
import Model.CardManagement as cm

import exceptions as exp

#Logging utility
import logging
logger = logging.getLogger(__name__)


class HeroManagement(cm.CardManagement):
    def __init__(self, *args):
        super().__init__(*args)
        self.managedDb = mod.Hero
    
    def __contains__(self, item):
        """Function that checks if a hero exists in the database
            - item : The item to match
            - return : True if the hero exists, False otherwise"""
        if not isinstance(item, mod.Hero): return False
        rawRes = self.session.query(mod.Hero).filter(mod.Hero.id == item.id)
        return len(list(rawRes)) != 0

    def create(self, name: str, birthdate: str, birthplace: mod.Box = None, content: str = "") -> mod.Hero:
        """ Create a new Hero card with the specified parameters.

        Args:
            name (str): The name of the new Hero card.
            birthdate (datetime.datetime): The birthdate of the new Hero.
            birthplace (str): The name of the Box where the new Hero card will be placed.
            content (str, optional): The content of the new Hero card. Defaults to an empty string.

        Returns:
            mod.Hero: The newly created Hero card.

        Raises:
            exp.CardException: If a birthdate is not provided.
        """
        parentCard = None
        # We check if the card already exists in the database
        if birthdate is None:
            logger.error("Error: When is {} born? No birthdate found".format(name))
            raise exp.CardException("No birthdate!", "Error: When is {} born?".format(name))

        # We check if a card exists.
        rawRes = self.session.query(mod.Card).filter(mod.Card.name == name)
        if len(list(rawRes)) != 0:
            parentCard = super().get(name = name)
            logger.info("Card : {} already exists. Creating Hero related to {}".format(name, name))
            
            # In the case where a Hero card exists, we raise an error. The only case where a Hero card exists is if a Card card exists attached to it.
            rawResHer = self.session.query(mod.Hero).filter(mod.Hero.id == parentCard.id)
            if len(list(rawResHer)) != 0:
                logger.error("Hero : {} already exists".format(name))
                raise exp.CardException("Existing Hero", "Error: Hero {} already exists".format(name))

        # If it doesn't exist, we create a new card.
        else:
            parentCard = super().create(title = name, box = birthplace, content = content)
            logger.info("Created new Card : {}".format(name))

        hero = mod.Hero(id = parentCard.id, birthdate = birthdate, card = parentCard)
        logger.info("Created new Hero : {}".format(name))
        self.session.add(hero)
        self.session.commit()

        return hero

    def get(self, id = None, name = None):
        """Method overriding the superclass get method for Hero management
        Args:
            - Parameters of the superclass"""
        if id is not None or name is not None:
            card = super().get(id = id, name = name)
            try:
                query = mod.Hero.get(self.session, id = card.id)
                logger.info("Retrieved Hero {} from the database".format(name))
                return query

            except ValueError as e:
                logger.error(str(e))
                raise exp.CardException("Card problem", str(e))
        
        query = self.session.query(mod.Hero)
        return query.all()

    def modify(self, nameHero: str, newName: str = None, content: str = None, newBday: dt = None, newBplace: str = None):
        """ Method that modifies an existing Hero card.

        Args:
            - nameHero (str): The name of the Hero card to modify.
            - newName (str): The new name for the Hero card.
            - content (str): The new content for the Hero card.
            - newBday (dt): The new birthdate for the Hero.
            - newBplace (str): The new Box for the Hero card.
        """
        # Check if the hero exists
        self.get(name = nameHero)

        if newBday:
            hero = self.session.query(mod.Hero).filter(mod.Hero.name == nameHero).first()
            hero.birthdate = newBday
            #self.session.commit()
            self.session.query(mod.Card).filter(mod.Card.name == nameHero).update({mod.Card.dtLastMod : dt.now()})
            self.session.commit()
            
            logger.info("Hero {} birthdate updated to {}".format(nameHero, newBday))

        if newBplace:
            super().move(nameHero, newBplace)
            card = self.session.query(mod.Card).filter(mod.Card.name == nameHero)
            hero = self.session.query(mod.Hero).filter(mod.Hero.id == card.id)
            hero.birthplace_id = card.group_id

            self.session.query(mod.Card).filter(mod.Card.name == nameHero).update({mod.Card.dtLastMod : dt.now()})
            self.session.commit()

            logger.info("Hero {} moved to {}".format(nameHero, newBplace))

        # Check if it's the hero that should be updated:
        if newName or content:
            super().modify(nameHero, newName, content)

    def delete(self, name):
        """Method overriding the superclass delete method for Hero management
            param : see Card param"""
        card = self.get(name = name)
        self.session.delete(card)
        logger.info("Deleting Hero {}".format(name))
        self.session.commit()
        super().delete(name)
