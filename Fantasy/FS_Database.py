import sqlalchemy as db

from Database.essential import *
from Services.Misc import DateParser

class Hero(Base):
    __tablename__ = "heros"

    id = db.Column(db.Integer, db.ForeignKey("cards.id"), primary_key=True)
    birthdate = db.Column(db.String(255), nullable=False)
    birthplace_id = db.Column(db.Integer, db.ForeignKey("states.id"))

    birthplace = db.orm.relationship("State", backref=db.orm.backref("heros", lazy=True))
    card = db.orm.relationship("Card")

    def as_dict(self):
        hero_dict = self.card.as_dict()
        hero_dict["birthdate"] = DateParser(self.birthdate)
        hero_dict["birthplace"] = None if self.birthplace is None else self.birthplace.name

        return hero_dict

    @classmethod
    def get(cls, session, id):
        # Query the database for a Box with the specified id
        item = session.query(cls).filter_by(id=id).first()
        return item

class State(Base):
    __tablename__ = "states"
    id = db.Column(db.Integer, db.ForeignKey("boxes.id"), primary_key=True)
    foundation = db.Column(db.String(255), nullable = False)
    capital = db.Column(db.String(255), nullable = False)

    alliegance = db.orm.relationship("Box")

    def as_dict(self):
        stateDict = self.alliegance.as_dict()
        stateDict["foundation"] = DateParser(self.foundation)
        stateDict["capital"] = self.capital

        return stateDict

    @classmethod
    def get(cls, session, id):
        # Query the database for a Box with the specified id
        item = session.query(cls).filter_by(id=id).first()
        return item
