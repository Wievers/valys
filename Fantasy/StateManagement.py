import os
from datetime import datetime as dt

import Fantasy.FS_Database as mod
import Model.BoxManagement as bm

import exceptions as exp

#Logging utility
import logging
logger = logging.getLogger(__name__)

class StateManagement(bm.BoxManagement):
    def __init__(self, *args):
        super().__init__(*args)
        self.managedDb = mod.State

    def __contains__(self, item):
        """Function that checks if a hero exists in the database
            - item : The item to match
            - return : True if the hero exists, False otherwise"""
        if not isinstance(item, mod.State): return False
        rawRes = self.session.query(mod.State).filter(mod.State.id == item.id)
        return len(list(rawRes)) != 0

    def create(self, name: str, foundation: dt, capital: str, descr: str = None, alliegance: str = None) -> mod.State:
        """ Creates a new state with the given name, foundation year, capital city, and optional description and alliegance.
        The new state is added to the database and a `State` object is returned.

        Parameters:
            name (str): The name of the state.
            foundation (dt): The foundation year of the state.
            capital (str): The capital city of the state.
            descr (str, optional): A description of the state. Defaults to None.
            alliegance (str, optional): The alliegance of the state. Defaults to None.

        Returns:
            State: The newly created state object.
        """
        if foundation is None:
            raise exp.BoxException("No foundation year!", "Error: When is {} created?".format(name))
            logger.error("Error: When is {} created? No year given".format(name))

        if capital is None:
            raise exp.BoxException("No capital!", "Error: What is the capital city of {}?".format(name))
            logger.error("Error: No capital for {}!".format(name))

        associatedBox = super().create(name, descr, alliegance)
        state = mod.State(id = associatedBox.id, foundation = foundation, capital = capital)
        self.session.add(state)
        self.session.commit()

        return state

    def get_children(self, name = None):
        """ Returns a list of all the children of the state with the given name."""
        children = super().get_children(name)
        finaleChilds = []
        for i in children:
            rawResHero = self.session.query(mod.Hero).filter(mod.Hero.id == i.id)
            rawResState = self.session.query(mod.State).filter(mod.State.id == i.id)
            if len(list(rawResHero)) or len(list(rawResState)):
                finaleChilds.append(i)

        return finaleChilds

    def modify(self, name: str, newName: str = None, newDescr: str = None, newFoundation: dt = None, newCapital: str = None, newAlliegance: str = None):
        """ Modify the attributes of a state.

        Args:
            name (str): The name of the state to modify.
            newName (str, optional): The new name of the state. Defaults to None.
            newDescr (str, optional): The new description of the state. Defaults to None.
            newFoundation (dt, optional): The new foundation date of the state. Defaults to None.
            newCapital (str, optional): The new capital of the state. Defaults to None.
            newAlliegance (str, optional): The new alliegance of the state. Defaults to None.
        """
        # Check if the state exists
        self.get(name = name)

        # Check if it's the state that should be updated:
        if newName or newDescr:
            super().modify(name, newName, newDescr)

        if newFoundation:
            self.session.query(mod.State).filter(mod.State.name == name).update({mod.State.foundation: newFoundation})
            self.session.commit()

        if newCapital:
            self.session.query(mod.State).filter(mod.State.name == name).update({mod.State.capital: newCapital})
            self.session.commit()

        if newAlliegance:
            super().move(name, newAlliegance)
