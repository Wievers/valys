from PySide2 import QtWidgets as qtw
from PySide2 import QtGui as qtg
from PySide2.QtCore import Qt, Slot

import Interface.SecondaryWidgets as prm

class ModifyDg(prm.ModifyDg):
    def __init__(self, parent, box = False, conversion = False):
        super().__init__(parent, box)
        layoutH = qtw.QHBoxLayout()
        self.day = qtw.QLineEdit()
        self.day.setValidator(qtg.QIntValidator(1, 30))
        self.day.setPlaceholderText("Jour")
        self.day.setReadOnly(True)
        self.month = qtw.QLineEdit()
        self.month.setValidator(qtg.QIntValidator(1, 15))
        self.month.setPlaceholderText("Mois")
        self.month.setReadOnly(True)
        self.year = qtw.QLineEdit()
        self.year.setValidator(qtg.QIntValidator())
        self.year.setPlaceholderText("Année")
        self.year.setReadOnly(True)

        if box:
            self.capital = qtw.QLineEdit()
            self.capital.setPlaceholderText("Capitale")
            self.capital.setReadOnly(True)
            self.layout().insertWidget(self.layout().count() - 1, self.capital)

        layoutH.addWidget(self.day)
        layoutH.addWidget(self.month)
        layoutH.addWidget(self.year)
        self.layout().insertLayout(self.layout().count() - 1, layoutH)

    @Slot()
    def modify(self):
        item = super().modify().as_dict()
        self.day.setReadOnly(False)
        self.month.setReadOnly(False)
        self.year.setReadOnly(False)

        if self.isBox:
            self.day.setText(item["foundation"].day)
            self.month.setText(item["foundation"].month)
            self.year.setText(item["foundation"].year)
            self.capital.setReadOnly(False)
            self.capital.setText(item["capital"])

        else:
            self.day.setText(item["birthdate"].day)
            self.month.setText(item["birthdate"].month)
            self.year.setText(item["birthdate"].year)


    @Slot()
    def save(self):
        descr = None if not self.isBox else self.descr.toPlainText()
        if self.isBox:
            self.parent.widget.modify(self.isBox, self.items.currentText(), {"newName" : self.name.text(),
                                        "newDescr" : descr,
                                        "newFoundation" : "{}/{}/{}".format(self.day.text(), self.month.text(), self.year.text()),
                                        "newCapital" : self.capital.text()})
        else:
            self.parent.widget.modify(self.isBox, self.items.currentText(), {"newName" : self.name.text(),
                                        "newBday" : "{}/{}/{}".format(self.day.text(), self.month.text(), self.year.text())})

        self.close()

class CreateDg(prm.CreateDg):
    def __init__(self, parent, box = False):
        super().__init__(parent, box)

        self.items = qtw.QComboBox()
        cards = parent.widget.get(False)
        self.items.addItems([i["name"] for i in cards])
        self.items.setEditable(True)
        self.items.setCompleter(qtw.QCompleter())
        self.items.lineEdit().setPlaceholderText("Nom")
        self.items.completer().setCompletionMode(qtw.QCompleter.PopupCompletion)
        self.items.setCurrentText("")

        layoutH = qtw.QHBoxLayout()
        self.day = qtw.QLineEdit()
        self.day.setValidator(qtg.QIntValidator(1, 30))
        self.day.setPlaceholderText("Jour")
        self.month = qtw.QLineEdit()
        self.month.setValidator(qtg.QIntValidator(1, 15))
        self.month.setPlaceholderText("Mois")
        self.year = qtw.QLineEdit()
        self.year.setValidator(qtg.QIntValidator())
        self.year.setPlaceholderText("Année")

        if box:
            self.capital = qtw.QLineEdit()
            self.capital.setPlaceholderText("Capitale")
            self.layout().insertWidget(self.layout().count() - 1, self.capital)

        layoutH.addWidget(self.day)
        layoutH.addWidget(self.month)
        layoutH.addWidget(self.year)
       
        self.title.setParent(None)
        self.layout().removeWidget(self.title)

        self.layout().insertWidget(0, self.items)
        self.layout().insertLayout(self.layout().count() - 1, layoutH)

    @Slot()
    def save(self):
        descr = None if self.descr is None else self.descr.toPlainText()
        boxParent = None if self.boxPater.currentText() == "" else self.boxPater.currentText()
        if self.isBox:
            self.parent.widget.create(self.isBox, self.title.text(), {"descr" : descr,
                                            "parent" : boxParent,
                                            "foundation" : "{}/{}/{}".format(self.day.text(), self.month.text(), self.year.text()),
                                            "capital" : self.capital.text()})
        else:
            self.parent.widget.create(self.isBox, self.title.text(), {"birthplace" : boxParent,
                                            "birthdate" : "{}/{}/{}".format(self.day.text(), self.month.text(), self.year.text())})

        self.close()

class OutputWidg(prm.OutputWidg):
    def __init__(self, parent, font = None):
        super().__init__(parent, font)

        self.birthdateLbl = qtw.QGroupBox("Date de naissance")
        self.birthdate = qtw.QLabel()

        layout = qtw.QHBoxLayout()
        layout.addWidget(self.birthdate)
        self.birthdateLbl.setLayout(layout)

        self.layout().insertWidget(0, self.birthdateLbl)

    def reset(self):
        """Method resetting text fields of the group box"""
        super().reset()
        self.birthdate.setText("")

    def setText(self, title, card):
        """Method setting the text of the outputwidget
            - date : DateParser object"""
        super().setText(title, card)
        self.birthdate.setText(card["birthdate"].toString())

class OpenDg(prm.OpenDg):
    """Class asking the input to find the sheet to open"""
    def __init__(self, parent, controller, delete = False, box = False, move = False):
        super().__init__(parent, controller, delete, box, move)
        if move:
            self.resLabel = qtw.QLabel("Héro à déplacer" if not box else "Etat à déplacer")
