import unittest

from Model.BoxManagement import *
from Database.essential import *

class TestBoxManagement(unittest.TestCase):
    def setUp(self):
        # Create a new instance of the BoxManagement class
        self.box_management = BoxManagement("sqlite:///:memory:")

        # Create some test boxes
        self.box1 = Box(id=1, name='Box 1', parent=None)
        self.box2 = Box(id=2, name='Box 2', parent=self.box1)
        self.box3 = Box(id=3, name='Box 3', parent=self.box1)
        self.box4 = Box(id=4, name='Box 4', parent=self.box3)
        self.box5 = Box(id=5, name='Box 5', parent=self.box4)
        self.box6 = Box(id=6, name='Box 6', parent=self.box5)

        # Add the test boxes to the database
        self.box_management.session.add(self.box1)
        self.box_management.session.add(self.box2)
        self.box_management.session.add(self.box3)
        self.box_management.session.add(self.box4)
        self.box_management.session.add(self.box5)
        self.box_management.session.add(self.box6)
        self.box_management.session.commit()

    def test_contains(self):
        box = Box(id=7, name='Box 7')
        self.assertTrue(self.box_management.__contains__(self.box2))
        self.assertFalse(self.box_management.__contains__(None))
        self.assertFalse(self.box_management.__contains__(box))

    def test_get(self):
        # Test retrieving all boxes
        boxes = self.box_management.get()
        self.assertEqual(len(boxes), 6)

        # Test retrieving a specific box by its name
        box = self.box_management.get(name='Box 5')
        self.assertEqual(box.name, 'Box 5')

    def test_create(self):
        # Test creating a new box
        self.box_management.create('New Box', parent='Box 1')
        new_box = self.box_management.get(name='New Box')
        self.assertEqual(new_box.parent.name, 'Box 1')

        # Test creating a new box with no parent
        self.box_management.create('Another New Box')
        another_new_box = self.box_management.get(name='Another New Box')
        self.assertIsNone(another_new_box.parent)

    def test_delete(self):
        # Test deleting a box with no children
        self.box_management.delete('Box 2')
        self.assertRaises(exp.BoxException, self.box_management.get, "Box 2")

if __name__ == '__main__':
    os.environ["DATABASE"] = "sqlite:///:memory:"
    os.environ["MODULE"] = "default"
    
    # Initialize the test suite
    suite = unittest.TestLoader().loadTestsFromTestCase(TestBoxManagement)

    # Run the test suite and print the results
    result = unittest.TextTestRunner(verbosity=2).run(suite)
    print(result)
