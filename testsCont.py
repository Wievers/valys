import unittest
from Services.controller import *
from Database.essential import *
from exceptions import *

from sqlalchemy.ext.declarative import declarative_base

import unittest
import sqlalchemy as db
from datetime import datetime as dt
import os

Base = declarative_base()

class TestController(unittest.TestCase):
    def setUp(self):
        # Set up a test database and create a Controller instance
        self.cm = Controller(True)
        # Add some mock boxes to the test database
        self.mock_boxes = [
            Box(id = 1, name="Box 1", description="description for box 1"),
            Box(id = 2, name="Box 2", description="description for box 2"),
            Box(id = 3, name="Box 3", description="description for box 3")
        ]
        self.cm.boxes.session.add_all(self.mock_boxes)
        self.cm.boxes.session.commit()

        # Add some mock cards to the test database
        self.mock_cards = [
            Card(id = 1, name="Card 1", content="Content for card 1", dtCreation = dt.now(), dtLastMod = dt.now()),
            Card(id = 2, name="Card 2", content="Content for card 2", dtCreation = dt.now(), dtLastMod = dt.now(), group = self.mock_boxes[0]),
            Card(id = 3, name="Card 3", content="Content for card 3", dtCreation = dt.now(), dtLastMod = dt.now(), group = self.mock_boxes[2])
        ]
        self.cm.cards.session.add_all(self.mock_cards)
        self.cm.cards.session.commit()

    def test_contains(self):
        card = Card(id = 4, name="Card 4", content="Content for card 1", dtCreation = dt.now(), dtLastMod = dt.now())
        self.assertTrue(self.cm.cards.__contains__(self.mock_cards[0]))
        self.assertFalse(self.cm.cards.__contains__(None))
        self.assertFalse(self.cm.cards.__contains__(card))

    def test_get(self):
        # Test that the get method returns the correct list of cards
        self.assertEqual(self.cm.get(None, False, "Card 1"), self.mock_cards[0].as_dict())
        self.assertEqual(self.cm.get(None, True, "Box 3"), self.mock_boxes[2].as_dict())

    def test_modify(self):
        # Test that the save method correctly updates the content of a card
        new_content = "Updated content for card 1"
        self.cm.modify(None, False, "Card 1", {"content" : new_content})
        self.assertEqual(self.cm.cards.session.query(Card).filter_by(name="Card 1").one().content, new_content)
        
        new_content2 = "Updated content for box 1"
        self.cm.modify(None, True, "Box 1", {"newDescr" : new_content2})
        self.assertEqual(self.cm.boxes.session.query(Box).filter_by(name = "Box 1").one().description, new_content2)
    
    def test_move(self):
        self.cm.move(None, "Card 1", "Box 1", False)
        self.assertEqual(self.cm.cards.session.query(Card).filter_by(name="Card 1").one().group, self.mock_boxes[0])

        self.cm.move(None, "Box 2", "Box 1", True)
        self.assertEqual(self.cm.boxes.session.query(Box).filter_by(name="Box 2").one().parent, self.mock_boxes[0])
        
    def test_open(self):
        # Test that the open method returns the correct content of a card
        self.assertEqual(self.cm.open(None, "Card 1"), self.mock_cards[0].as_dict())

    def test_create(self):
        # Test that the create method adds a new card to the database
        self.cm.create(None, False, "Card 4", {"content" : "Content for card 4"})
        self.assertEqual(len(self.cm.get(None, False)), len(self.mock_cards) + 1)

        self.cm.create(None, True, "Box 4", {"descr" : "Content for box 4"})
        self.assertEqual(len(self.cm.get(None, True)), len(self.mock_boxes) + 1)

    """def test_move_moves_card_to_new_box(self):
        # Test that the move method correctly moves a card to a new box
        new_box = "Box 1"
        self.cm.move("Card 1", new_box)
        self.assertEqual(self.session.query(Card).filter_by(name="Card 1").one().box, new_box)"""

    def test_delete(self):
        # Test that the delete method correctly removes a card from the database
        self.cm.cards.delete("Card 1")
        mockCards = [i.as_dict() for i in self.mock_cards]
        self.assertEqual(self.cm.get(None, False), mockCards[1:])

if __name__ == '__main__':
    os.environ["DATABASE"] = "sqlite:///:memory:"
    os.environ["MODULE"] = "default"
    # Initialize the test suite
    suite = unittest.TestLoader().loadTestsFromTestCase(TestController)

    # Run the test suite and print the results
    result = unittest.TextTestRunner(verbosity=2).run(suite)
    print(result)
