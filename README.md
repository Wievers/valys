# Valys
Valys Note Manager is a python developed note organizer. The version currently on the git is the v2.0.

## Description
It's purpose is to enhance normal editors by adding links between the notes (Basically making notes' "tree"). This can be useful for related courses/genealogy and so on.\\
It is intended to work alongside the SRP for backups, but also fluidity (between computers). A mobile API of both programs is currently being created (Kotlin).

## Installation & Licence
Valys requires PySide2 to work properly, although, with some work, PyQt5 should do just fine (There is some differences tho).
Sharing the same spirit as PySide2 and Open Software, this program is licensed under the GPLv3.
Licence: http://www.gnu.org/licenses/gpl-3.0.html

## Support
If you need anything, have any question, you can use either the bug request (But i'm not too much used to it, being essentially a student in physics) or my university email:
paul.monkam-daverat@imt-atlantique.net

## Authors and acknowledgment
Paul MONKAM-DAVERAT

## Project status
This project is under constant development for now.
