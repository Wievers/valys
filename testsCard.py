import unittest
from Model.CardManagement import *
from Database.essential import *
from exceptions import *

from sqlalchemy.ext.declarative import declarative_base

import unittest
import sqlalchemy as db
from datetime import datetime as dt
import os

Base = declarative_base()

class TestCardManagement(unittest.TestCase):
    def setUp(self):
        # Set up a test database and create a CardManagement instance
        self.cm = CardManagement()
        # Add some mock cards to the test database
        self.mock_cards = [
            Card(id = 1, name="Card 1", content="Content for card 1", dtCreation = dt.now(), dtLastMod = dt.now()),
            Card(id = 2, name="Card 2", content="Content for card 2", dtCreation = dt.now(), dtLastMod = dt.now()),
            Card(id = 3, name="Card 3", content="Content for card 3", dtCreation = dt.now(), dtLastMod = dt.now())
        ]
        self.cm.session.add_all(self.mock_cards)
        self.cm.session.commit()

    def test_contains(self):
        card = Card(id = 4, name="Card 4", content="Content for card 1", dtCreation = dt.now(), dtLastMod = dt.now())
        self.assertTrue(self.cm.__contains__(self.mock_cards[0]))
        self.assertFalse(self.cm.__contains__(None))
        self.assertFalse(self.cm.__contains__(card))

    def test_get_returns_all_cards(self):
        # Test that the get method returns the correct list of cards
        self.assertEqual(self.cm.get(), self.mock_cards)

    def test_save_updates_card_content(self):
        # Test that the save method correctly updates the content of a card
        new_content = "Updated content for card 1"
        self.cm.modify("Card 1", content = new_content)
        self.assertEqual(self.cm.session.query(Card).filter_by(name="Card 1").one().content, new_content)

    def test_open_returns_card_content(self):
        # Test that the open method returns the correct content of a card
        self.assertEqual(self.cm.open("Card 1"), self.mock_cards[0].as_dict())

    def test_create_adds_new_card_to_database(self):
        # Test that the create method adds a new card to the database
        self.cm.create("Card 4", "Content for card 4")

        self.assertRaises(CardException, self.cm.create, "Card 4", "Content for card 4")
        self.assertEqual(len(self.cm.get()), len(self.mock_cards) + 1)

    def test_modify_updates_card_content(self):
        # Test that the modify method correctly updates the content of a card
        new_content = "Updated content for card 1"
        self.cm.modify("Card 1", content=new_content)
        self.assertEqual(self.cm.session.query(Card).filter_by(name="Card 1").one().content, new_content)

    """def test_move_moves_card_to_new_box(self):
        # Test that the move method correctly moves a card to a new box
        new_box = "Box 1"
        self.cm.move("Card 1", new_box)
        self.assertEqual(self.session.query(Card).filter_by(name="Card 1").one().box, new_box)"""

    def test_delete_removes_card_from_database(self):
        # Test that the delete method correctly removes a card from the database
        self.cm.delete("Card 1")
        self.assertEqual(self.cm.get(), self.mock_cards[1:])

    def test_search_returns_matching_card(self):
        # Test that the search method returns the correct card that matches the search criteria
        self.assertEqual(self.cm.get(name="Card 1"), self.mock_cards[0])

if __name__ == '__main__':
    os.environ["DATABASE"] = "sqlite:///:memory:"
    os.environ["MODULE"] = "default"
    # Initialize the test suite
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCardManagement)

    # Run the test suite and print the results
    result = unittest.TextTestRunner(verbosity=2).run(suite)
    print(result)
